<?php
/**
 * @package ncms
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 16.04.14
 */

namespace ncms;


/**
 * Автозагрузка классов
 * @package ncms
 */
class autoloader {

 /**
  * Инициализация автозагрузки
  * @throws \LogicException в случае отсутвтсия класса
  */
 public static function attach()
 {
  set_include_path(get_include_path().PATH_SEPARATOR.__DIR__.'/../');
  spl_autoload_extensions(".php");
  spl_autoload_register();
 }
}