<?php
/**
 * @package ncms_discounts
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 17.04.14
 */

namespace ncms\discounts;


/**
 * Проверяет, попадают ли товары из заказа под скидку определенного типа.
 * 
 * Скидка на товары, если в заказе количество товаров не меньше чем $products_count
 * 
 * @package ncms\discounts
 */
class discount_products_count_checker extends a_discount_checker {

 /**
  * Проверяет, попадают ли товары из заказа под скидку
  * @param \ncms\discounts\a_discount $discount конкретный объект
  * @param \ncms\orders\i_order $order объект заказа
  * @return boolean
  */
 public function check(\ncms\discounts\a_discount $discount, \ncms\orders\i_order $order)
 {
  $ignore_products = $discount->get_ignore_products();
  $discount_amount = 0;
  $action_products = array();


  foreach ($order->get_products() as $product)
  {
   if (!in_array($product, $ignore_products))
   {
    $action_products[] = $product;
    $discount_amount += $product->get_price();
   }
  }

  
  if (count($action_products) >= $discount->get_products_count())
  {
   $this->set_action_products($action_products)->set_amount_discount($discount_amount / 100 * $discount->get_discount());
   return TRUE;
  }
  else
  {
   $this->set_action_products(NULL)->set_amount_discount(0);
   return FALSE;
  }
 }
}