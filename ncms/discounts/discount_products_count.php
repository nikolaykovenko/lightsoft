<?php
/**
 * @package ncms_discounts
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 17.04.14
 */

namespace ncms\discounts;


/**
 * Скидка на товары, если в заказе количество товаров не меньше чем $products_count
 * @package ncms\discounts
 */
class discount_products_count extends a_discount {

 /**
  * @var int минимальное количество товаров
  */
 protected $products_count = 0;

 /**
  * Устанавливает минимальное количество товаров
  * @param int $products_count
  * @return $this
  */
 public function set_products_count($products_count)
 {
  $this->products_count = (int)$products_count;
  return $this;
 }

 /**
  * Возвращает минимальное количество товаров
  * @return int
  */
 public function get_products_count()
 {
  return $this->products_count;
 }

 
}