<?php
/**
 * @package ncms_discounts
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 17.04.14
 */

namespace ncms\discounts;


/**
 * Проверяет, попадают ли товары из заказа под скидку определенного типа.
 * 
 * Скидка на один из товаров, выбранных вместе с товаром X
 * 
 * @package ncms\discounts
 */
class discount_selected_product_and_set_checker extends a_discount_checker {

 /**
  * Проверяет, попадают ли товары из заказа под скидку
  * @param \ncms\discounts\a_discount $discount конкретный объект
  * @param \ncms\orders\i_order $order объект заказа
  * @return boolean
  */
 public function check(\ncms\discounts\a_discount $discount, \ncms\orders\i_order $order)
 {
  $order_products = $order->get_products();
  $discount_products = $discount->get_products();
  
  $result = FALSE;
  if (in_array($discount->get_selected_product(), $order_products))
  {
   $i = 0;
   $count = count($discount_products);
   
   while ($result == FALSE and $i < $count)
   {
    if (in_array($discount_products[$i], $order_products)) $result = TRUE; else $i++;
   }
  }
  

  if ($result)
   $this->set_action_products(array($discount_products[$i]))->set_amount_discount($discount_products[$i]->get_price() / 100 * $discount->get_discount());
  else $this->set_action_products(NULL)->set_amount_discount(0);

  return $result;
 }
}