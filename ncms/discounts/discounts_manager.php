<?php
/**
 * @package ncms_discounts
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 16.04.14
 */

namespace ncms\discounts;

/**
 * Менеджер скидок
 * @package ncms\discounts
 */
class discounts_manager implements i_discounts_manager {

 /**
  * @var array массив доступных скидок
  */
 protected $discounts = array();

 /**
  * Добавить скидку
  * @param i_discount $discount
  * @return $this
  */
 public function add_discount(i_discount $discount)
 {
  if (!in_array($discount, $this->discounts)) $this->discounts[] = $discount;
  return $this;
 }


 /**
  * Возвращает массив скидок
  * @return array
  */
 public function get_discounts()
 {
  return $this->discounts;
 }
}