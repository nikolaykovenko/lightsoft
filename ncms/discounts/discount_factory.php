<?php
/**
 * @package ncms_discounts
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 17.04.14
 */

namespace ncms\discounts;




/**
 * Фабрика для генерации обьектов скидок и их объектов-"чекеров" (оъектов для проверки попадания под условия скидок)
 * @package ncms\discounts
 */
class discount_factory {

 static protected $namespace = '\ncms\discounts\\';

 /**
  * Генерация объекта скидки
  * @param string $discount_type
  * @param float $discount_value
  * @throws \LogicException в случае неправильного типа скидки
  * @return \ncms\discounts\i_discount
  */
 static public function get_discount($discount_type, $discount_value)
 {
  $class_name = self::$namespace.$discount_type;
  if (class_exists($class_name)) return new $class_name($discount_value);
 }

 /**
  * @param i_discount $discount
  * @throws \LogicException в случае неправильного типа скидки
  * @return \ncms\discounts\a_discount_checker
  */
 static public function get_checker(i_discount $discount)
 {
  $class_name = get_class($discount).'_checker';
  if (class_exists($class_name)) return new $class_name(); 
 }

}