<?php
/**
 * @package ncms_discounts
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 17.04.14
 */

namespace ncms\discounts;


/**
 * Проверяет, попадают ли товары из заказа под скидку определенного типа.
 * 
 * Скидка, которая распространяется на набор заданных товаров
 * 
 * @package ncms\discounts
 */
class discount_products_set_checker extends a_discount_checker {

 /**
  * Проверяет, попадают ли товары из заказа под скидку
  * @param \ncms\discounts\a_discount $discount конкретный объект
  * @param \ncms\orders\i_order $order объект заказа
  * @return boolean
  */
 public function check(\ncms\discounts\a_discount $discount, \ncms\orders\i_order $order)
 {
  $discount_products = $discount->get_products();
  $discount_amount = 0;
  
  $result = TRUE;
  foreach ($discount_products as $product)
  {
   if (!in_array($product, $order->get_products())) $result = FALSE;
   $discount_amount += $product->get_price();
  }
  
  if ($result) $this->set_action_products($discount_products)->set_amount_discount($discount_amount / 100 * $discount->get_discount()); else $this->set_action_products(NULL)->set_amount_discount(0);
  
  return $result;
 }
}