<?php
/**
 * @package ncms_discounts
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 16.04.14
 */

namespace ncms\discounts;

/**
 * Общий интерфейс для всех скидок
 * @package ncms\discounts
 */
interface i_discount {

 /**
  * Устанавливает размер скидки (в %)
  * @param float $discount
  * @return $this
  */
 public function set_discount($discount);

 /**
  * Возвращает размер скидки
  * @return float
  */
 public function get_discount();

 /**
  * Добавляет переденные товары в список товаров, которые попадают под условия скидки
  * 
  * Принимает произвольное количество параметров типа \ncms\products\i_product
  * 
  * @return $this
  */
 public function set_products();

 /**
  * Возвращает массив товаров, попадающих под условия скидки
  * @return array
  */
 public function get_products();

 /**
  * Добавляет в список товары, которые не попадают под условия скидки
  * 
  * Принимает произвольное количество параметров типа \ncms\products\i_product
  * 
  * @return $this
  */
 public function set_ignore_products();

 /**
  * Возвращает список товаров, которые не попадают под условия скидки
  * @return array
  */
 public function get_ignore_products();
}