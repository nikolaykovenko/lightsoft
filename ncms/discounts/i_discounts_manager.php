<?php
/**
 * @package ncms_discounts
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 16.04.14
 */

namespace ncms\discounts;


/**
 * Интерфейс менеджера скидок
 * @package ncms\discounts
 */
interface i_discounts_manager {

 /**
  * Добавить скидку
  * @param i_discount $discount
  * @return $this
  */
 public function add_discount(i_discount $discount);

 /**
  * Возвращает массив скидок
  * @return array
  */
 public function get_discounts();
 
}