<?php
/**
 * @package ncms_discounts
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 17.04.14
 */

namespace ncms\discounts;


/**
 * Скидка на один из товаров, выбранных вместе с товаром X
 * @package ncms\discounts
 */
class discount_selected_product_and_set extends a_discount {

 /**
  * @var \ncms\products\i_product выбранный товар
  */
 protected $selected_product;


 /**
  * Устанавливает товар X
  * @param \ncms\products\i_product $product
  * @return $this
  */
 public function set_selected_good(\ncms\products\i_product $product)
 {
  $this->selected_product = $product;
  return $this;
 }

 /**
  * Возвращает товар X
  * @return \ncms\products\i_product
  */
 public function get_selected_product()
 {
  return $this->selected_product;
 }
}