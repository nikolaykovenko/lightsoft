<?php
/**
 * @package ncms_discounts
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 17.04.14
 */

namespace ncms\discounts;


/**
 * Проверяет, попадают ли товары из заказа под скидку определенного типа.
 * @package ncms\discounts
 */
abstract class a_discount_checker {

 /**
  * @var array массив товаров, которые подпали под акцию во время последней проверки
  */
 protected $action_products = array();

 /**
  * @var int размер скидки в валюте системы после пследней проверки
  */
 protected $amount_discount = 0;

 /**
  * Устанавливает массив товаров, которые подпали под акцию во время последней проверки
  * @param array $products
  * @return $this
  */
 public function set_action_products($products)
 {
  if (is_array($products)) $this->action_products = $products; else $this->action_products = array();
  return $this;
 }

 /**
  * Возвращает массив товаров, которые подпали под акцию во время последней проверки
  * @return array
  */
 public function get_action_products()
 {
  return $this->action_products;
 }

 /**
  * Устанавливает размер скидки в валюте системы после последней проверки
  * @param float $amount
  * @return $this
  */
 public function set_amount_discount($amount)
 {
  $this->amount_discount = (float)$amount;
  return $this;
 }

 /**
  * Возвращает размер скидки в валюте системы после последней проверки
  * @return int
  */
 public function get_amount_discount()
 {
  return $this->amount_discount;
 }


 /**
  * Проверяет, попадают ли товары из заказа под скидку
  * @param \ncms\discounts\a_discount $discount конкретный объект
  * @param \ncms\orders\i_order $order объект заказа
  * @return boolean
  */
 abstract public function check(\ncms\discounts\a_discount $discount, \ncms\orders\i_order $order);
 
}