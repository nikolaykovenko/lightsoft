<?php
/**
 * @package ncms_discounts
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 17.04.14
 */

namespace ncms\discounts;


/**
 * Абстратктный класс с общим функционалом для всех видов скидок
 * @package ncms\discounts
 */
abstract class a_discount implements i_discount {

 /**
  * @var float размер скидки
  */
 protected $discount = 0;

 /**
  * @var array товары, попадающие под скидку
  */
 protected $products = array();

 /**
  * @var array товары, не по падающие под скидку
  */
 protected $ignore_products = array();


 /**
  * Конструктор
  * @param float $discount размер скидки (в %)
  */
 public function __construct($discount)
 {
  $this->set_discount($discount);
 }

 /**
  * Устанавливает размер скидки (в %)
  * @param float $discount
  * @return $this
  */
 public function set_discount($discount)
 {
  $this->discount = (float)$discount;
  return $this;
 }

 /**
  * Возвращает размер скидки
  * @return float
  */
 public function get_discount()
 {
  return $this->discount;
 }

 /**
  * Добавляет переденные товары в список товаров, которые попадают под условия скидки.
  * 
  * Принимает произвольное количество параметров типа \ncms\products\i_product 
  * 
  * @return $this
  */
 public function set_products()
 {
  foreach (func_get_args() as $product)
  {
   if ($product instanceof \ncms\products\i_product and !in_array($product, $this->products)) $this->products[] = $product;
  }
  
  return $this;
 }

 /**
  * Возвращает массив товаров, попадающих под условия скидки
  * @return array
  */
 public function get_products()
 {
  return $this->products;
 }

 /**
  * Добавляет в список товары, которые не попадают под условия скидки
  * 
  * Принимает произвольное количество параметров типа \ncms\products\i_product
  * 
  * @return $this
  */
 public function set_ignore_products()
 {
  foreach (func_get_args() as $product)
  {
   if ($product instanceof \ncms\products\i_product and !in_array($product, $this->ignore_products)) $this->ignore_products[] = $product;
  }

  return $this;
 }

 /**
  * Возвращает список товаров, которые не попадают под условия скидки
  * @return array
  */
 public function get_ignore_products()
 {
  return $this->ignore_products;
 }
}