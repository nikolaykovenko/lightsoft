<?php
/**
 * @package ncms_calculator
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 16.04.14
 */

namespace ncms\calculator;


/**
 * Калькулятор стоимости заказа на основе заказа и доступных скидок
 * @package ncms\calculator
 */
class calculator {

 /**
  * @var \ncms\orders\i_order заказ
  */
 protected $order;

 /**
  * @var \ncms\discounts\i_discounts_manager|NULL менеджер скидок
  */
 protected $discounts_manager;

 /**
  * @var \ncms\discounts\discount_factory фабрика скидок;
  */
 protected $discount_factory;

 /**
  * @var float цена заказа без скидки
  */
 protected $total_amount = 0;

 /**
  * Конструктор
  * @param \ncms\orders\i_order $order заказ
  * @param \ncms\discounts\discount_factory $discount_factory
  */
 public function __construct(\ncms\orders\i_order $order, \ncms\discounts\discount_factory $discount_factory)
 {
  $this->order = clone $order; // клонируем объект заказа, так как будем удалять из него товары по мере попадания их под критерии скидок
  $this->total_amount = $order->total_price();
  $this->discount_factory = $discount_factory;
 }

 /**
  * Устанавливает менеджер скидок
  * @param \ncms\discounts\i_discounts_manager $manager
  * @return $this
  */
 public function set_discount_manager(\ncms\discounts\i_discounts_manager $manager)
 {
  $this->discounts_manager = $manager;
  return $this;
 }
 
 /**
  * Вычисление стоимости товаров, учитывая доступные скидки
  * @throws \Exception в случае непредвиденной ошибки вычисления
  * @return float
  */
 public function calculate()
 {
  $total_discount = 0;
  
  if (!is_null($this->discounts_manager))
  {
   foreach ($this->discounts_manager->get_discounts() as $discount)
   {
    $checker = $this->discount_factory->get_checker($discount);
    if ($checker->check($discount, $this->order))
    {
     $total_discount += $checker->get_amount_discount();
     foreach ($checker->get_action_products() as $product) $this->order->remove_product($product);
     
//     echo 'discount ok '.$discount->get_discount().', td = '.$total_discount.' pc = '.count($this->order->get_products()).'<br>';
    }
   }
  }

  return $this->total_amount - $total_discount;
 }
}