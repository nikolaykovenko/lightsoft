<?php
/**
 * @package ncms_orders
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 16.04.14
 */

namespace ncms\orders;

/**
 * Интерфейс заказа
 * @package ncms\orders
 */
interface i_order {
 /**
  * Добавить продукт
  * @param \ncms\products\i_product $product
  * @return $this
  */
 public function add_product(\ncms\products\i_product $product);

 /**
  * Удалить продукт
  * @param \ncms\products\i_product $product
  * @return bool
  */
 public function remove_product(\ncms\products\i_product $product);

 /**
  * Возвращает список товаров
  * @return array
  */
 public function get_products();

 /**
  * Возвращает стоимость товаров заказа
  * @return float
  */
 public function total_price();
}