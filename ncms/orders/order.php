<?php
/**
 * @package ncms_orders
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 16.04.14
 */

namespace ncms\orders;


/**
 * Корзина заказа
 * @package ncms\orders
 */
class order implements i_order {

 /**
  * @var array массив товаров
  */
 protected $products = array();

 /**
  * Добавить продукт
  * @param \ncms\products\i_product $product
  * @return $this
  */
 public function add_product(\ncms\products\i_product $product)
 {
  $this->products[] = $product;
  return $this;
 }

 /**
  * Удалить продукт
  * @param \ncms\products\i_product $product
  * @return bool TRUE, если продукт был найден в списке продуктов заказа.
  */
 public function remove_product(\ncms\products\i_product $product)
 {
  $result = FALSE;
  $new_product = array();
  foreach ($this->products as $_pr) if ($result === FALSE and $_pr === $product) $result = TRUE; else $new_product[] = $_pr;
  $this->products = $new_product;
  return $this;
 }

 /**
  * Возвращает список товаров
  * @return array
  */
 public function get_products()
 {
  return $this->products;
 }

 /**
  * Возвращает стоимость товаров заказа
  * @return float
  */
 public function total_price()
 {
  $result = 0;
  foreach ($this->get_products() as $product) $result += $product->get_price();
  return $result;
 }
}