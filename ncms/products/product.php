<?php
/**
 * @package ncms_shop
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 16.04.14
 */

namespace ncms\products;

/**
 * Товар
 * @package ncms\products
 */
class product implements i_product {

 /**
  * @var string идентификатор товара
  */
 protected $id;

 /**
  * @var float цена товара
  */
 protected $price;

 /**
  * Конструктор
  * @param string $good_id идентификатор товара
  * @param float $good_price цена товара
  */
 public function __construct($good_id, $good_price)
 {
  $this->set_id($good_id);
  $this->set_price($good_price);
 }

 /**
  * Устанавливает цену
  * @param float $price
  * @return $this
  */
 public function set_price($price)
 {
  $this->price = (float)$price;
  return $this;
 }

 /**
  * Возвращает цену
  * @return float
  */
 public function get_price()
 {
  return (float)$this->price;
 }

 /**
  * Устанавливает идентификатор
  * @param string $id
  * @return $this
  */
 public function set_id($id)
 {
  $this->id = (string)$id;
  return $this;
 }

 /**
  * Возвращает идентификатор
  * @return string
  */
 public function get_id()
 {
  return $this->id;
 }
}