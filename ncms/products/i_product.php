<?php
/**
 * @package ncms_discounts
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 16.04.14
 */

namespace ncms\products;

/**
 * Товар
 * @package ncms\products
 */
interface i_product {

 /**
  * Устанавливает цену
  * @param float $price
  * @return $this
  */
 public function set_price($price);

 /**
  * Возвращает цену
  * @return float
  */
 public function get_price();

 /**
  * Устанавливает идентификатор
  * @param string $id
  * @return $this
  */
 public function set_id($id);

 /**
  * Возвращает идентификатор
  * @return string
  */
 public function get_id();
}