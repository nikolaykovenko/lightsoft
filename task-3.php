<?php
/**
 * @package ncms
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 17.04.14
 */


/**
 * Проверяет строку на корректность расстановки скобок
 * @param string $check_str строка для проверки
 * @return bool
 */
function isCorrect($check_str)
{
 $count = 1;
 while ($count > 0) $check_str = preg_replace(array("/[\(][\)]/", "/[\{][\}]/"), '', $check_str, -1, $count);
 return empty($check_str);
}

assert(isCorrect('') === true);
assert(isCorrect('()') === true);
assert(isCorrect('{()}') === true);
assert(isCorrect('{()}{}') === true);
assert(isCorrect('(())') === true);
assert(isCorrect('{({({({()})})})}') === true);
assert(isCorrect('{(})') === false);

?> 