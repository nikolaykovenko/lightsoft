<?php
/**
 * @package ncms
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 16.04.14
 */

namespace ncms;

use ncms\discounts\discount_factory;
use ncms\discounts\discount_products_count;
use ncms\discounts\discount_products_set;
use ncms\discounts\discount_selected_product_and_set;

require_once 'ncms/autoloader.php';
autoloader::attach();


/*
 * Список всех товаров
 */
$productA = new products\product('A', 100);
$productB = new products\product('B', 200);
$productC = new products\product('C', 300);
$productD = new products\product('D', 400);
$productE = new products\product('E', 500);
$productF = new products\product('F', 600);
$productG = new products\product('G', 700);
$productH = new products\product('H', 800);
$productI = new products\product('I', 900);
$productJ = new products\product('J', 1000);
$productK = new products\product('K', 1100);
$productL = new products\product('L', 1200);
$productM = new products\product('M', 1300);


/*
 * Формирование заказа
 */
$order = new orders\order();
$order->add_product($productA);
$order->add_product($productB);
$order->add_product($productC);
$order->add_product($productD);
$order->add_product($productE);
$order->add_product($productF);
$order->add_product($productG);
$order->add_product($productH);
$order->add_product($productI);
$order->add_product($productJ);
$order->add_product($productK);
$order->add_product($productL);
$order->add_product($productM);


/*
 * Формирование скидок
 */
$discount_factory = new discount_factory();

$discounts = array();

try {
 $discounts[1] = $discount_factory::get_discount('discount_products_set', 10);
 $discounts[1]->set_products($productA, $productB);

 $discounts[2] = $discount_factory::get_discount('discount_products_set', 5);
 $discounts[2]->set_products($productD, $productE);

 $discounts[3] = $discount_factory::get_discount('discount_products_set', 6);
 $discounts[3]->set_products($productE, $productF, $productG);

 $discounts[4] = $discount_factory::get_discount('discount_selected_product_and_set', 50);
 $discounts[4]->set_selected_good($productA)->set_products($productK, $productL, $productM);

 $discounts[5] = $discount_factory::get_discount('discount_products_count', 20);
 $discounts[5]->set_products_count(5)->set_ignore_products($productA, $productC);

 $discounts[6] = $discount_factory::get_discount('discount_products_count', 10);
 $discounts[6]->set_products_count(4)->set_ignore_products($productA, $productC);

 $discounts[7] = $discount_factory::get_discount('discount_products_count', 5);
 $discounts[7]->set_products_count(3)->set_ignore_products($productA, $productC);
}
catch (\Exception $e)
{
 die('object error');
}




/*
 * Менеджер скидок
 */
$discount_manager = new discounts\discounts_manager();
foreach ($discounts as $discount) $discount_manager->add_discount($discount);


/*
 * Кальулятор стоимости
 */
$calculator = new calculator\calculator($order, $discount_factory);
$calculator->set_discount_manager($discount_manager);
try {
 $total_amount = $calculator->calculate();
}
catch (\Exception $e)
{
 $total_amount = 'Discount calculation error =(';
}



/*
 * Предстваление результатов
 */
echo 'Price without discounts - '.$order->total_price().'<br>
      Price with discounts - '.$total_amount;

?>