<?php
/**
 * @package default
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 24.04.14
 */

class Item {
 /** @var mixed */
 public $value;

 /**
  * Содержит пары "название" => подэлемент.
  * @var Item[]
  */
 public $subItems = array();

 public function __construct($value, array $subItems = array())
 {
  $this->value = $value;
  $this->subItems = $subItems;
 }
}


$root = new Item(10,
    array(
    'sub1' => new Item(11),
    'sub2' => new Item(12, array(
            'sub3' => new Item(11),
            'sub4' => new Item(15)
        )),
    'sub3' => new Item(18),
));




function printTree(Item $root, $level = 0)
{
 echo $level.': '.$root->value.'<br>';
 if (!empty($root->subItems)) foreach ($root->subItems as $item) printTree($item, $level + 1); 
}

function search(Item $root, $value, $path = '')
{
 if ($root->value == $value) return (empty($path) ? '/' : $path);
 elseif (!empty($root->subItems))
 {
  foreach ($root->subItems as $index => $item)
  {
   if ($result = search($item, $value, $path.'/'.$index)) return $result;
  }
 }
 else return NULL;
}

printTree($root);

assert(search($root, 15) == '/sub2/sub4');
assert(search($root, 17) === null);
assert(search($root, 11) == '/sub1');
assert(search($root, 18) == '/sub3');
assert(search($root, 10) == '/');

?> 